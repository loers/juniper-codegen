extern crate proc_macro;
use std::{fs::read_to_string, path::PathBuf};

use convert_case::{Case, Casing};
use graphql_parser::schema::{Definition, InputValue, TypeDefinition};
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, punctuated::Punctuated, Token};

#[proc_macro]
pub fn generate(item: TokenStream) -> TokenStream {
    let schema_file_path = parse_macro_input!(item as syn::LitStr);
    let cargo_dir =
        std::env::var("CARGO_MANIFEST_DIR").expect("Env var `CARGO_MANIFEST_DIR` was missing");
    let pwd = PathBuf::from(cargo_dir);
    let schema_path = pwd.join(schema_file_path.clone().value());
    let schema =
        read_to_string(&schema_path).expect(&format!("Failed: could not read {:?}.", schema_path));
    let schema = graphql_parser::parse_schema::<String>(&schema)
        .unwrap()
        .to_owned();

    let mut items = Vec::<syn::Item>::new();
    for def in schema.definitions {
        match def {
            Definition::SchemaDefinition(_) => {}
            Definition::TypeDefinition(type_definition) => {
                match type_definition {
                    TypeDefinition::Object(object) => {
                        let name = syn::Ident::new(&object.name, schema_file_path.span());
                        let fields = generate_fields(&name, &object.fields)
                            .expect("Failed to generate fields");
                        let item: syn::Item = syn::parse(
                            quote!(
                                #[derive(Debug, Clone)]
                                #[allow(dead_code)]
                                pub struct #name #fields
                            )
                            .into(),
                        )
                        .expect("Failed to generate struct");
                        items.push(item);

                        let impl_methods = generate_methods(&name, &object.fields)
                            .expect("Failed to generate methods");
                        let impl_item: syn::Item = syn::parse(
                            quote!(
                                #[graphql_object(context = Context)]
                                impl #name {
                                    #(#impl_methods)*
                                }
                            )
                            .into(),
                        )
                        .expect("Failed to generate impl block");

                        items.push(impl_item);
                    }
                    TypeDefinition::Scalar(_) => {}
                    TypeDefinition::Interface(_) => {}
                    TypeDefinition::Union(_) => {}
                    TypeDefinition::Enum(_) => {}
                    TypeDefinition::InputObject(_) => {}
                }
                //
            }
            Definition::TypeExtension(_) => {}
            Definition::DirectiveDefinition(_) => {}
        }
    }

    let gen = quote! {
        #(#items)*
    };
    TokenStream::from(gen)
}

fn generate_arguments(
    input: &syn::Ident,
    arguments: &Vec<InputValue<String>>,
) -> Result<Punctuated<syn::FnArg, Token![,]>, syn::Error> {
    let mut args = Punctuated::<syn::FnArg, Token![,]>::new();

    for arg in arguments {
        let name = syn::Ident::new(&arg.name.to_case(Case::Snake), input.span());
        let typ = generate_type(input, &arg.value_type)?;
        args.push(syn::parse(
            quote!(
                #name: #typ
            )
            .into(),
        )?);
    }

    Ok(args)
}

fn generate_arguments_delegations(
    input: &syn::Ident,
    arguments: &Vec<InputValue<String>>,
) -> Result<Punctuated<syn::Ident, Token![,]>, syn::Error> {
    let mut args = Punctuated::<syn::Ident, Token![,]>::new();

    for a in arguments {
        let name = syn::Ident::new(&a.name.to_case(Case::Snake), input.span());
        args.push(syn::parse(
            quote!(
                #name
            )
            .into(),
        )?);
    }

    Ok(args)
}

fn generate_methods(
    input: &syn::Ident,
    fields: &Vec<graphql_parser::schema::Field<String>>,
) -> Result<Vec<syn::ImplItemMethod>, syn::Error> {
    let mut methods = Vec::<syn::ImplItemMethod>::new();
    let resolver_name = quote::format_ident!("{}Resolver", input);
    for field in fields {
        let arguments = generate_arguments(input, &field.arguments)?;
        let argument_delegations = generate_arguments_delegations(input, &field.arguments)?;

        let name = syn::Ident::new(&field.name.to_case(convert_case::Case::Snake), input.span());
        let ty = generate_type(input, &field.field_type)?;
        if let Some(_directive) = get_resolve_directive(&field.directives) {
            methods.push(syn::parse(
                quote!(
                    async fn #name(&self, context: &Context, #arguments) -> juniper::FieldResult<#ty> {
                        #resolver_name::#name(context, &self, #argument_delegations).await
                    }
                )
                .into(),
            )?);
            continue;
        }
        methods.push(syn::parse(
            quote!(
                async fn #name(&self, context: &Context) -> &#ty {
                    &self.#name
                }
            )
            .into(),
        )?);
    }
    Ok(methods)
}

struct ResolveDirective;

fn get_resolve_directive(
    directives: &Vec<graphql_parser::schema::Directive<String>>,
) -> Option<ResolveDirective> {
    directives
        .into_iter()
        .find(|d| d.name == "resolve")
        .map(|_d| ResolveDirective)
}

fn generate_fields(
    input: &syn::Ident,
    fields: &Vec<graphql_parser::schema::Field<String>>,
) -> Result<syn::FieldsNamed, syn::Error> {
    let mut gen = quote!();
    for field in fields {
        if let Some(_directive) = get_resolve_directive(&field.directives) {
            continue;
        }
        let field_name = field.name.to_case(convert_case::Case::Snake);
        let field_name = syn::Ident::new(&field_name, input.span());
        let ty = generate_type(input, &field.field_type)?;
        gen.extend(quote!(
            pub(crate) #field_name: #ty,
        ));
    }
    syn::parse(
        quote!(
            {
                #gen
            }
        )
        .into(),
    )
}

fn generate_type(
    input: &syn::Ident,
    typ: &graphql_parser::schema::Type<String>,
) -> Result<syn::Type, syn::Error> {
    match typ {
        graphql_parser::schema::Type::NamedType(name) => {
            let name = syn::Ident::new(name, input.span());
            syn::parse(
                quote!(
                    Option<#name>
                )
                .into(),
            )
        }
        graphql_parser::schema::Type::ListType(inner_type) => {
            let inner = generate_type(input, inner_type.as_ref())?;
            syn::parse(
                quote!(
                    Option<Vec<#inner>>
                )
                .into(),
            )
        }
        graphql_parser::schema::Type::NonNullType(inner_type) => {
            let inner = generate_type(input, inner_type.as_ref())?;
            let inner = extract_type_from_optional(&inner);
            syn::parse(
                quote!(
                    #inner
                )
                .into(),
            )
        }
    }
}

fn extract_type_from_optional<'a>(typ: &'a syn::Type) -> &'a syn::Type {
    match typ {
        syn::Type::Path(p) => {
            let arguments = &p.path.segments.first().unwrap().arguments;
            match arguments {
                syn::PathArguments::AngleBracketed(args) => match args.args.first().unwrap() {
                    syn::GenericArgument::Type(typ) => typ,
                    _ => unreachable!("Got field with unexpected type arguments."),
                },
                _ => unreachable!("Got field with unexpected arguments."),
            }
        }
        typ => typ,
    }
}

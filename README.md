# Juniper CodeGen

This crate provides the macro `generate!(...)` which generates juniper schema implementation code for a given schema.

# Example

```graphql
# src/schema.graphql
directive @gen(field: Boolean!) on FIELD_DEFINITION # ignore this directive for now. It will be explained later

schema {
    query: Query
    mutation: Mutation
}

type Mutation {
    createPerson(name: String!): Person! @gen(field: false) # again ignore the usages of the directive. It will be explained later.
    createDog(name: String!, owner: ID!) @gen(field: false)
}

type Query {
    list: [Person!]!
}

type Person {
    id: ID!
    name: String!
    age: Int!
    alive: Boolean!
    firends: [Person!]! @gen(field: false)
    dog: Dog @gen(field: false)
}

type Dog {
    id: ID!
    name: String!
    friends: [Dog!]! @gen(field: false)
    owner: Person! @gen(field: false)
}

```

To support this schema with juniper you need (roughly) the following code:

```rs
struct Person {
    id: ID,
    name: String,
    age: i32,
    alive: bool,
    dog: Dog
}

#[graphql_object(context = Context)]
impl Person {
    async fn id(&self) -> &ID {
        &self.id
    }
    async fn name(&self) -> &String {...}
    async fn age(...) ...
    async fn alive(...) ...

    async fn friends(&self, context: &Context) -> Vec<Person> {
        // get friends von db via context
    }

    async fn dog(&self, context: &Context) -> Option<Dog> {
        // get dog from context
    }
}

struct Dog {
    id: ID,
    name: String,
}
#[graphql_object(context = Context)]
impl Dog {
    async fn id(..) ...
    async fn name(...) ...

    async fn owner(&self, context: &Context) {
        // get owner via context
    }

    async fn friends(&self, context: &Context) {
        //
    }
}

struct Query;
#[graphql_object(context = Context)]
impl Query {
    fn list(&self, context: &Context) {
        ...
    }
}
struct Mutation;
#[graphql_object(context = Context)]
impl Mutation {
    ...
}

// The code above this line is the code we want to generate

pub type Schema = RootNode<'static, Query, Mutation, EmptySubscription<Context>>;

pub fn schema() -> Schema {
    Schema::new(Query {}, Mutation {}, EmptySubscription::<Context>::new())
}

```

Instead of writing the whole code we can use the `generate` macro to generate the example above. As you can see there are two types of functions in the impl blocks: Ones that are just getters for fields -> we can generate these. And the ones that retrieve data (via context) and provide owned data.

The latter ones will generate code that expects structs called `<type name>Resolver` with functions named `<type name>Resolver::<field name>`. To do so we need to mark them as "not being fields". Thus the directive `@gen(field: false)`.

```rs

// Lets define some types used in graphql:
type ID = String; // You could also just import junpiers ID type.
type Int = i32;
type Boolean = bool;

// juniper_codegen does not translate any types but excepts you to specify the types. Thus juniper_codegen is kept simple and small.

juniper_codegen::generate!("src/schema.graphql"); // Note the path needs to start at your crate root.

// Note: The generated code also expects the context struct to be named "Context".

struct PersonResolver;
impl PersonResolver {
    async fn friends(context: &Context, person: &Person) -> Vec<Person> {
        // How you implement these functions is completely up to you. Usually there is some database in the context to load data from.
        context.friends(person)
    }
    async fn dog(context: &Context, person: &Person) -> Option<Dog> {
        context.dog(person)
    }
}

struct DogResolver;
impl DogResolver {
    async fn owner(context: &Context, dog: &Dog) -> Person {
        context.owner(dog)
    }
 }
struct QueryResolver;
impl QueryResolver {
    async fn list(context: &Context, _: &Query) -> Vec<Person> {
        context.list()
    }
}
struct MutationResolver;
impl MutationResolver {
    async fn create_person(context: &Context, _: &Mutation, name: String) -> Person {
        context.create_person(name)
    }
    async fn create_dog(context: &Context, _: &Mutation, name: String, owner: ID) -> Dog {
        context.create_dog(name, owner)
    }
}

// We still need to concat everything and export a schema function.
pub type Schema = RootNode<'static, Query, Mutation, EmptySubscription<Context>>;
pub fn schema() -> Schema {
    Schema::new(Query {}, Mutation {}, EmptySubscription::<Context>::new())
}
```

This way we can reduce the boilerplate code put most work into the parts of code that need specific implementation anyways.

## State of Crate

This crate is in its infancy.

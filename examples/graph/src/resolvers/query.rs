use juniper::FieldResult;

use crate::schema::{Context, Int, Query, Vertex};

pub struct QueryResolver;
impl QueryResolver {
    pub async fn vertices(context: &Context, _: &Query, limit: Int) -> FieldResult<Vec<Vertex>> {
        Ok(context
            .vertices
            .read()
            .await
            .values()
            .enumerate()
            .filter_map(|(i, v)| {
                if i < limit as usize {
                    Some(v.clone())
                } else {
                    None
                }
            })
            .collect())
    }
}

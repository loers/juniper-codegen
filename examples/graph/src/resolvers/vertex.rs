use juniper::FieldResult;

use crate::schema::{Context, Edge, Vertex};

pub struct VertexResolver;
impl VertexResolver {
    pub async fn edges(context: &Context, vertex: &Vertex) -> FieldResult<Vec<Edge>> {
        Ok(context
            .edges
            .read()
            .await
            .values()
            .filter(|e| e.from_id == vertex.id)
            .cloned()
            .collect())
    }
}

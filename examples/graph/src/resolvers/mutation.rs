use juniper::FieldResult;
use uuid::Uuid;

use crate::schema::{Context, Edge, Mutation, Vertex, ID};

pub struct MutationResolver;
impl MutationResolver {
    pub async fn create_vertex(
        context: &Context,
        _: &Mutation,
        vertex_type: String,
        uri: Option<String>,
    ) -> FieldResult<Vertex> {
        let id = Uuid::new_v4().to_string();
        let vertex = Vertex {
            id: id.clone(),
            uri,
            labels: vec![vertex_type],
            neighbors: 0,
        };
        context
            .vertices
            .write()
            .await
            .insert(id.into(), vertex.clone());
        Ok(vertex)
    }

    pub async fn create_edge(
        context: &Context,
        _: &Mutation,
        from: ID,
        label: String,
        to: ID,
    ) -> FieldResult<Edge> {
        let id = Uuid::new_v4().to_string();
        let edge = Edge {
            id: id.clone(),
            label,
            from_id: from.into(),
            to_id: to.into(),
        };
        context.edges.write().await.insert(id.into(), edge.clone());
        Ok(edge)
    }
}

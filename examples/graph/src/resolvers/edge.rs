use juniper::{FieldError, FieldResult};

use crate::schema::{Context, Edge, Vertex};

pub struct EdgeResolver;
impl EdgeResolver {
    pub async fn from(context: &Context, edge: &Edge) -> FieldResult<Vertex> {
        if let Some(v) = context
            .vertices
            .read()
            .await
            .get(&edge.from_id.clone().into())
        {
            Ok(v.clone())
        } else {
            FieldResult::Err(FieldError::from(""))
        }
    }
    pub async fn to(context: &Context, edge: &Edge) -> FieldResult<Vertex> {
        if let Some(v) = context
            .vertices
            .read()
            .await
            .get(&edge.to_id.clone().into())
        {
            Ok(v.clone())
        } else {
            FieldResult::Err(FieldError::from(""))
        }
    }
}

use std::{collections::HashMap, sync::Arc};

use crate::schema::{Context, Schema};
use actix_web::{
    middleware,
    web::{self, Data},
    App, Error, HttpResponse, HttpServer,
};
use juniper_actix::{graphiql_handler, graphql_handler, playground_handler};
use schema::{schema, Edge, EdgeID, Vertex, VertexID};
use tokio::sync::RwLock;
use uuid::Uuid;

mod resolvers;
mod schema;

async fn graphiql_route() -> Result<HttpResponse, Error> {
    graphiql_handler("/graphql", None).await
}
async fn playground_route() -> Result<HttpResponse, Error> {
    playground_handler("/graphql", None).await
}

async fn graphql_route(
    req: actix_web::HttpRequest,
    payload: actix_web::web::Payload,
    context: web::Data<Context>,
    schema: web::Data<Schema>,
) -> Result<HttpResponse, Error> {
    graphql_handler(&schema, &context, req, payload).await
}

#[actix_web::main]
pub async fn main() {
    let (vertices, edges) = mock_data();
    let context = crate::schema::Context {
        vertices: Arc::new(RwLock::new(vertices)),
        edges: Arc::new(RwLock::new(edges)),
    };

    let server = HttpServer::new(move || {
        App::new()
            .app_data(Data::new(schema()))
            .app_data(Data::new(context.clone()))
            .wrap(middleware::Compress::default())
            .service(
                web::resource("/graphql")
                    .route(web::post().to(graphql_route))
                    .route(web::get().to(graphql_route)),
            )
            .service(web::resource("/playground").route(web::get().to(playground_route)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql_route)))
    });
    server.bind("127.0.0.1:8082").unwrap().run().await.unwrap();
}

fn mock_data() -> (HashMap<VertexID, Vertex>, HashMap<EdgeID, Edge>) {
    let mut vertices = HashMap::new();

    let vertex1 = Uuid::new_v4().to_string();
    vertices.insert(
        vertex1.clone().into(),
        Vertex {
            id: vertex1.clone(),
            uri: Some("https://example.com/foo/bar/1".into()),
            labels: vec!["foo".into(), "bar".into()],
            neighbors: 1,
        },
    );
    let vertex2 = Uuid::new_v4().to_string();
    vertices.insert(
        vertex2.clone().into(),
        Vertex {
            id: vertex2.clone().into(),
            uri: Some("https://example.com/foo/bar/2".into()),
            labels: vec!["foo2".into(), "bar2".into()],
            neighbors: 1,
        },
    );
    let mut edges = HashMap::new();
    let edge = Uuid::new_v4().to_string();
    edges.insert(
        edge.clone().into(),
        Edge {
            id: edge.clone().into(),
            label: "knows".into(),
            from_id: vertex1.clone(),
            to_id: vertex2.clone(),
        },
    );
    (vertices, edges)
}

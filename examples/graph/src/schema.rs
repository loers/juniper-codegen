use juniper::{graphql_object, EmptyMutation, EmptySubscription, RootNode};
use std::{collections::HashMap, sync::Arc};
use tokio::sync::RwLock;

use crate::resolvers::{EdgeResolver, MutationResolver, QueryResolver, VertexResolver};

pub type Schema = RootNode<'static, Query, EmptyMutation<Context>, EmptySubscription<Context>>;

pub fn schema() -> Schema {
    Schema::new(
        Query {},
        EmptyMutation::new(),
        EmptySubscription::<Context>::new(),
    )
}

pub(crate) type ID = String;
pub(crate) type Int = i32;
juniper_codegen::generate!("graph.graphql");

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Hash)]
pub struct VertexID(ID);
impl From<String> for VertexID {
    fn from(s: String) -> Self {
        Self(s)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, PartialOrd, Hash)]
pub struct EdgeID(ID);
impl From<String> for EdgeID {
    fn from(s: String) -> Self {
        Self(s)
    }
}

#[derive(Debug, Clone)]
pub struct Context {
    pub vertices: Arc<RwLock<HashMap<VertexID, Vertex>>>,
    pub edges: Arc<RwLock<HashMap<EdgeID, Edge>>>,
}

impl juniper::Context for Context {}
